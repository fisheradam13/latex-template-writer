import os
from tkinter.filedialog import askopenfilename
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QLineEdit, QMessageBox, QFileDialog, QComboBox, QHBoxLayout
from PyQt5.QtGui import QIcon, QStandardItemModel, QStandardItem, QFont
from PyQt5.QtCore import pyqtSlot
from os.path import expanduser
import sys
import json

# remove the following after "os.getcwd()" to use pyinstaller:  + os.sep + os.pardir
cd = os.path.normpath(os.getcwd() + os.sep + os.pardir)
tp = cd + '/Template'

formats = '(Select)','APA 6th Edition','MLA','Chicago'

apatemplatepath = tp + '/APAtemplate.tex'
mlatemplatepath = tp + '/MLAtemplate.tex'
chicagotemplatepath = tp + '/chicagotemplate.tex'
templatebibpath = tp + '/bibfile.bib'

a = open(apatemplatepath, 'r')
apatemplate = a.read()
a.close()

a2 = open(templatebibpath, 'r')
templatebib = a2.read()
a2.close()

a3 = open(mlatemplatepath, 'r')
mlatemplate = a3.read()
a3.close()

a4 = open(chicagotemplatepath, 'r')
chicagotemplate = a4.read()
a4.close()

chars1 = str(' < > : " / \ | ? * ')
chars = set('<>:"/\|?*')

input_dir = "N/A"

def parse_json_array(JSONarray, search_item):
    list = []
    for item in JSONarray:
        item = item[search_item]
        return(item)
    return list


with open('attributes.txt', 'r') as json_file:
    data = json.load(json_file)
    y = data['paper attributes']


authors = parse_json_array(y, 'Authors')
course = parse_json_array(y, 'Course')
folder_title = parse_json_array(y, 'Folder Name')
header_title = parse_json_array(y, 'Header Title')
institution = parse_json_array(y, 'Institution')
pdf_title = parse_json_array(y, 'PDF title')
professor = parse_json_array(y, 'Professor')
paper_title = parse_json_array(y, 'Paper Title')


class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = 'LaTeX File Properites'
        self.left = 460
        self.top = 290
        self.width = 1000
        self.height = 500
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # Create Format Selector

        self.label1 = QtWidgets.QLabel("Paper Format", self)
        self.label1.setGeometry(20, 20, 120, 40)

        self.model = QStandardItemModel()

        self.comboformats = QtWidgets.QComboBox(self)
        self.comboformats.setGeometry(140, 20, 200, 40)

        self.comboformats.setFont(QFont('', 7))
        self.comboformats.setModel(self.model)

        for x in formats:
            format = QStandardItem(x)
            self.model.appendRow(format)

        self.comboformats.currentIndexChanged.connect(self.hider)

        # Create textbox

        self.textbox1 = QLineEdit(folder_title, self)
        self.textbox1.setGeometry(140, 80, 280, 40)

        self.label1 = QtWidgets.QLabel("New Folder Name", self)
        self.label1.setGeometry(20, 80, 120, 40)

        self.textbox2 = QLineEdit(pdf_title, self)
        self.textbox2.setGeometry(140, 140, 280, 40)

        self.label2 = QtWidgets.QLabel("New PDF File Name", self)
        self.label2.setGeometry(20, 140, 120, 40)

        self.textbox3 = QLineEdit(authors, self)
        self.textbox3.setGeometry(140, 200, 280, 40)

        self.label3 = QtWidgets.QLabel("Paper Authors", self)
        self.label3.setGeometry(20, 200, 120, 40)

        self.textbox4 = QLineEdit(course, self)
        self.textbox4.setGeometry(140, 260, 280, 40)

        self.label4 = QtWidgets.QLabel("Course Name", self)
        self.label4.setGeometry(20, 260, 120, 40)

        self.textbox5 = QLineEdit(paper_title, self)
        self.textbox5.setGeometry(630, 80, 280, 40)

        self.label5 = QtWidgets.QLabel("Paper Title", self)
        self.label5.setGeometry(450, 80, 120, 40)

        self.textbox6 = QLineEdit(header_title, self)
        self.textbox6.setGeometry(630, 140, 280, 40)

        self.label6 = QtWidgets.QLabel("Shortened Title for Headers", self)
        self.label6.setGeometry(450, 140, 170, 40)

        self.textbox7 = QLineEdit(professor, self)
        self.textbox7.setGeometry(630, 200, 280, 40)

        self.label7 = QtWidgets.QLabel("Professor Name", self)
        self.label7.setGeometry(450, 200, 120, 40)

        self.textbox8 = QLineEdit(institution, self)
        self.textbox8.setGeometry(630, 260, 280, 40)

        self.label8 = QtWidgets.QLabel("Institution Name", self)
        self.label8.setGeometry(450, 260, 120, 40)

        # Create a button in the window

        self.button1 = QPushButton('Folder Location', self)
        self.button1.move(20, 320)

        self.file_path_label = QtWidgets.QLabel(input_dir, self)
        self.file_path_label.setGeometry(140, 315, 600, 40)

        self.folder_error_label = QtWidgets.QLabel(self)
        self.folder_error_label.setGeometry(190, 345, 600, 40)


        self.button2 = QPushButton('Create LaTeX Template', self)
        self.button2.setGeometry(20, 360, 150, 30)

        # connect button to function on_click

        self.button1.clicked.connect(self.on_click1)
        self.button2.clicked.connect(self.on_click2)

        self.show()

    @pyqtSlot()

    def hider(self):
        self.selected = self.comboformats.currentText()

        if self.selected == 'MLA':
            self.textbox6.setHidden(True)
            self.label6.setHidden(True)
        elif self.selected == 'Chicago':
            self.textbox6.setHidden(True)
            self.label6.setHidden(True)
        elif self.selected == 'APA 6th Edition':
            self.textbox6.setHidden(False)
            self.label6.setHidden(False)


    def on_click1(self):
        global input_dir
        input_dir = str(QFileDialog.getExistingDirectory(self, 'Select a folder:', expanduser("~")))
        self.file_path_label.setText(input_dir)

    def on_click2(self):

        self.selected = self.comboformats.currentText()

        if input_dir == "N/A":
            self.folder_error_label.setStyleSheet("QLabel {color: red;}")
            self.folder_error_label.setText("!! No folder path selected !!")

        elif input_dir == "":
            self.folder_error_label.setStyleSheet("QLabel {color: red;}")
            self.folder_error_label.setText("!! No folder path selected !!")

        elif self.selected == '(Select)':
            self.folder_error_label.setStyleSheet("QLabel {color: red;}")
            self.folder_error_label.setText("!! No paper format selected !!")

        else:
            self.newpath = input_dir + '/' + self.textbox1.text()
            self.newbibpath = input_dir + '/' + self.textbox1.text() + '/bibfile.bib'
            self.newpdfname = self.newpath + '/' + self.textbox2.text() + '.tex'

            self.authorsfile = self.newpath + '/Files/Authors.txt'
            self.authorslastfile = self.newpath + '/Files/AuthorsLast.txt'
            self.coursefile = self.newpath + '/Files/Course.txt'
            self.headertitlefile = self.newpath + '/Files/Headertitle.txt'
            self.capsheadertitlefile = self.newpath + '/Files/CapsHeadertitle.txt'
            self.institutionfile = self.newpath + '/Files/Institution.txt'
            self.professorfile = self.newpath + '/Files/Professor.txt'
            self.papertitlefile = self.newpath + '/Files/Title.txt'
            if os.path.exists(self.newpath):
                self.folder_error_label.setStyleSheet("QLabel {color: red;}")
                self.folder_error_label.setText("!! Folder already exists !!")

            elif any((c in chars) for c in self.textbox1.text()):
                self.folder_error_label.setStyleSheet("QLabel {color: red;}")
                self.folder_error_label.setText("!! Unaccepted character used in folder name !! (Ex: " + chars1 + ")")

            elif any((c in chars) for c in self.textbox2.text()):
                self.folder_error_label.setStyleSheet("QLabel {color: red;}")
                self.folder_error_label.setText("!! Unaccepted character used in .tex file name !! (Ex: " + chars1 + ")")

            else:
                os.makedirs(self.newpath)
                os.makedirs(self.newpath + '/Images')
                os.makedirs(self.newpath + '/Appendices')
                os.makedirs(self.newpath + '/Files')

                if self.selected == 'APA 6th Edition':
                    b = open(self.newpdfname, 'w')
                    b.write(apatemplate)
                    b.close()

                elif self.selected == 'MLA':
                    b = open(self.newpdfname, 'w')
                    b.write(mlatemplate)
                    b.close()

                elif self.selected == 'Chicago':
                    b = open(self.newpdfname, 'w')
                    b.write(chicagotemplate)
                    b.close()

                c = open(self.authorsfile, 'w')
                c.write(self.textbox3.text())
                c.close()

                newauthor = self.textbox3.text()
                newauthorlast = newauthor.split(' ')

                c2 = open(self.authorslastfile, 'w')
                c2.write(newauthorlast[-1])
                c2.close()

                d = open(self.coursefile, 'w')
                d.write(self.textbox4.text())
                d.close()

                e = open(self.headertitlefile, 'w')
                e.write(self.textbox6.text())
                e.close()

                f = open(self.capsheadertitlefile, 'w')
                f.write("\MakeUppercase{" + self.textbox6.text() + "}")
                f.close()

                g = open(self.institutionfile, 'w')
                g.write(self.textbox8.text())
                g.close()

                h = open(self.professorfile, 'w')
                h.write(self.textbox7.text())
                h.close()

                i = open(self.papertitlefile, 'w')
                i.write(self.textbox5.text())
                i.close()

                j = open(self.newbibpath, 'w')
                j.write(templatebib)
                j.close()

                data2 = {}
                data2['paper attributes'] = []
                data2['paper attributes'].append({
                    'Authors': self.textbox3.text(),
                    'Course': self.textbox4.text(),
                    'Folder Name': self.textbox1.text(),
                    'Header Title': self.textbox6.text(),
                    'Institution': self.textbox8.text(),
                    'PDF title': self.textbox2.text(),
                    'Professor': self.textbox7.text(),
                    'Paper Title': self.textbox5.text(),
                })

                with open('attributes.txt', 'w') as outfile:
                    json.dump(data2, outfile)

                self.folder_error_label.setStyleSheet("QLabel {color: green;}")
                self.folder_error_label.setText("Done!")

                os.startfile(self.newpath)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())